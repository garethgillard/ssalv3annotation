# Salmo salar V3 rapid release annotation

Linking new rapid release of the Salmo salar (Ssal) annotation of V3 genome assembly to previous gene names and descriptions (V2).

New annotation: https://rapid.ensembl.org/Salmo_salar_GCA_905237065.2/Info/Index  
Gff: http://ftp.ensembl.org/pub/rapid-release/species/Salmo_salar/GCA_905237065.2/geneset/2021_07/Salmo_salar-GCA_905237065.2-2021_07-genes.gff3.gz  

Previous annotation data from NCBI release, available through: https://gitlab.com/sandve-lab/salmonfisher

Links between new and old annotation based on best blastn hits between V3 cdna and V2 rna from genomic.  
V3 cdna: http://ftp.ensembl.org/pub/rapid-release/species/Salmo_salar/GCA_905237065.2/geneset/2021_07/Salmo_salar-GCA_905237065.2-2021_07-cdna.fa.gz  
V2 rna: https://ftp.ncbi.nlm.nih.gov/genomes/all/GCF/000/233/375/GCF_000233375.1_ICSASG_v2/GCF_000233375.1_ICSASG_v2_rna_from_genomic.fna.gz  

Resulting table combining blast results with previous annotation data: Salmo_salar-GCA_905237065.2_gene_annotations.tsv

# Column info

Columns from previous assembly annotation is prefaced with 'v2', and columns without this preface can be assumed from the new V3 assembly.

**gene_id**: Ensembl gene id from new V3 release gff.  
**version**: Version of gene id, 2 (new) or 1 (previous).  
**chr**, **start**, **end**, **strand**: Chromosome location of gene from V3 gff.  
**symbol**: Gene symbol generated from a machine learning classifier.  
**symbol_probability**: Probability statistic of symbol.  
**transcript_id**: Ensembl transcript id for the gene from V3 gff.  
**v2.gene_id.NCBI**: NCBI gene id of the best matching gene from previous V2 gff.  
**v2.transcript_id.NCBI**: NCBI transcript id of V2 rna best matching new V3 cdna.  
**bitscore**, **evalue**, **pident**: Blast stats of the hit between transcripts.  
**v2.gene_id.ensembl**: Ensembl gene id linked to the V2 NCBI gene id from biomart.  
**v2.gene_name.ensembl**: Ensembl gene name, V2.  
**v2.gene_name**: NCBI gene name, V2.  
**v2.product**: NCBI protein product description, from longest isoform, V2.  
**v2.chr**, **v2.start**, **v2.end**, **v2.strand**: Chromosome location of V2 gene.  
**ensembl_ids_match**: Do V3 and V2 ensembl ids match (without version number).  
**chr_match**: Do V3 and V2 chromosome numbers match (proper chr numbers only).  
**symbols_match**: Do predicted symbols match V2 ensembl gene name (lowercases).  

# Column statistics

Plot shows number of gene rows that contain  non-missing (or non-NA) values for each column of data, and the number of rows where a pair of column values match where we expect.

![Annotation stats](annotation_table_stats.png)

